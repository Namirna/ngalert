var app = angular.module('');

app.directive('ngAlertWarning', function () {
    return {
        restrict: 'A',
        replace: true,
        transclude: true,
        templateUrl: 'Scripts/Application/Directives/NgAlert/alert.html',
        scope: {
            show: "="
        },
        link: function (scope, element, attrs) {
            handleAlert(scope, element, attrs, "alert-warning");
        }
    };
});

function handleAlert(scope, element, attrs, typeClass) {
    element.addClass(typeClass);

    if (attrs.dismissable != null) {

        element.find('button').click(function () {
            scope.show = false;
            scope.$apply();
        });
    } else {
        element.find('button').addClass('hidden');
        element.removeClass('alert-dismissable');
    }
}

app.directive('ngAlertError', function () {
    return {
        restrict: 'A',
        replace: true,
        transclude: true,
        templateUrl: 'Scripts/Application/Directives/NgAlert/alert.html',
        scope: {
            show: "="
        },
        link: function (scope, element, attrs) {
            handleAlert(scope, element, attrs, "alert-danger");
        }
    };
});

app.directive('ngAlertSuccess', function () {
    return {
        restrict: 'A',
        replace: true,
        transclude: true,
        templateUrl: 'Scripts/Application/Directives/NgAlert/alert.html',
        scope: {
            show: "="
        },
        link: function (scope, element, attrs) {
            handleAlert(scope, element, attrs, "alert-success");
        }
    };
});

app.directive('ngAlertInfo', function () {
    return {
        restrict: 'A',
        replace: true,
        transclude: true,
        templateUrl: 'Scripts/Application/Directives/NgAlert/alert.html',
        scope: {
            show: "="
        },
        link: function (scope, element, attrs) {
            handleAlert(scope, element, attrs, "alert-info");
        }
    };
});